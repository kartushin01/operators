﻿using System;

namespace Operators
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            var pac1 = new Package();
            pac1.SetLength(50);
            pac1.SetHeight(10);
            pac1.SetWidth(35);
            pac1.SetWeight(1000);

            var pac2 = new Package(70, 35, 15, 3000);
            var pac3 = new Package();

            pac3 = pac1 + pac2;
            var pac4 = pac3;

            var volumePac3 = pac3.GetVolume();

            Console.WriteLine($"Объём посылки1 : {pac1}");
            Console.WriteLine($"Объём посылки2 : {pac2}");
            Console.WriteLine($"Объём посылки3 : {pac3}");

            if (pac3 == pac4)
            {
                Console.WriteLine("Посылки 3 и 4 равны");
            }
            else
            {
                Console.WriteLine("Посылки 3 и 4 не равны");
            }

            pac1[0] = new Thing() { Name = "pencil" };
            pac1[1] = new Thing() { Name = "laptop" };
            pac1[2] = new Thing() { Name = "smartphone" };

            Console.WriteLine($"Индексатор : {pac1[0].Name}");
            Console.WriteLine($"Индексатор : {pac1[1].Name}");
            Console.WriteLine($"Индексатор : {pac1[2].Name}");

            string testSting = "Учимся расширять классы";

            int CntWord = testSting.CountWord();

            Console.WriteLine( $" В фразе {testSting} : {CntWord} { Helper.GetDeclension(CntWord, "слово","слова", "слов")} ");

            Console.ReadKey();

        }
    }
}
