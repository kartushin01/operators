﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Operators
{
    /// <summary>
    /// Класс для работы с сущностью почтовых посылок. Сравнение по длинне, ширене, объёму и весу.
    /// </summary>
    public class Package
    {
        private Thing[] subjects;

        private double length;
        private double width;
        private double height;
        private double weight;

        public Package(double length, double width, double height, double weight)
        {
            this.length = length;
            this.width = width;
            this.height = height;
            this.weight = weight;
        }

        /// <summary>
        /// Индексатор
        /// </summary>
        public Thing this[int index]
        {
            get => subjects[index];
            set => subjects[index] = value;
        }

        public Package()
        {
            subjects = new Thing[5];
        }

        /// <summary>
        /// Метод для вычисления объёма посылки
        /// </summary>
        public double GetVolume()
        {
            return length * width * height;
        }

        public void SetLength(double l)
        {
            length = l;
        }

        public void SetWidth(double wi)
        {
            width = wi;
        }

        public void SetWeight(double we)
        {
            weight = we;
        }

        public void SetHeight(double h)
        {
            height = h;
        }

        public static Package operator +(Package a, Package b)
        {
            var pac = new Package();
            pac.width = a.width + b.width;
            pac.height = a.height + b.height;
            pac.length = a.length + b.length;
            pac.weight = a.weight + b.weight;
            return pac;
        }

        public static bool operator ==(Package a, Package b)
        {
            var res = false;

            if (a.length == b.length && a.height == b.height && a.width == b.width && a.weight != b.weight)
            {
                res = true;
            }
            return res;
        }

        public static bool operator !=(Package a, Package b)
        {
            var res = false;

            if (a.length != b.length || a.height != b.height || a.width != b.width || a.weight != b.weight)
            {
                res = true;
            }
            return res;
        }

        /// <summary>
        /// перегрузка для класса ToString
        /// </summary>
        public override string ToString()
        {
            return $"Длинна: {length}; ширина:{width}; высота:{height}; вес: {weight}";
        }
    }
}