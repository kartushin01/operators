﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Operators
{

    /// <summary>
    /// Раширение класса string
    /// </summary>
    public static class StringExtension
    {  
        /// <summary>
        /// Метод для подсчёта слов строке
        /// </summary>
        public static int CountWord(this string str)
        {
            return str.Split(new char[] { ' ', '.', '?', '!' },
                StringSplitOptions.RemoveEmptyEntries).Length;
        }
    }
}
